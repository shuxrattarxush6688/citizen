package uz.bank.citizenservice.controller;
import org.springframework.web.bind.annotation.*;
import uz.bank.citizenservice.dto.CitizenDto;
import uz.bank.citizenservice.service.CitizenInfoService;

@RestController
@RequestMapping("api/v1/citizen")
public class CitizenController {

    private final CitizenInfoService service;

    public CitizenController(CitizenInfoService service) {
        this.service = service;
    }

    @PostMapping
    public String add(@RequestBody CitizenDto dto) {
        service.add(dto);
        return "Ma'lumot qo'shildi";
    }

    @GetMapping
    public CitizenDto getOne(@RequestParam String number, @RequestParam String serial) {
        return service.findOne(serial, number);
    }


}
