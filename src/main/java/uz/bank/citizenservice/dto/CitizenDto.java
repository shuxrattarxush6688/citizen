package uz.bank.citizenservice.dto;

import ch.qos.logback.core.joran.spi.NoAutoStart;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.bank.citizenservice.entity.Citizen;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CitizenDto {

    private String passportSeries;

    private String passportNumber;

    private String name;

    private String surname;

    private String birthDate;

    public static CitizenDto toDto(Citizen citizen) {
        return new CitizenDto(
                citizen.getPassportSeries(),
                citizen.getPassportNumber(),
                citizen.getName(),
                citizen.getSurname(),
                citizen.getBirthDate()
        );
    }

    public static Citizen toEntity(CitizenDto dto){
        Citizen citizen = new Citizen();
        citizen.setName(dto.getName());
        citizen.setSurname(dto.getSurname());
        citizen.setBirthDate(dto.getBirthDate());
        citizen.setPassportSeries(dto.getPassportSeries());
        citizen.setPassportNumber(dto.getPassportNumber());

        return citizen;
    }
}
