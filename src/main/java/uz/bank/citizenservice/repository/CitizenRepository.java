package uz.bank.citizenservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.bank.citizenservice.entity.Citizen;

public interface CitizenRepository extends JpaRepository<Citizen, Long> {
    Citizen findCitizenByPassportSeriesAndPassportNumber(String seria, String number);
}
