package uz.bank.citizenservice.service;
import uz.bank.citizenservice.dto.CitizenDto;

public interface CitizenInfoService {

    void add(CitizenDto dto);
    CitizenDto findOne(String serial, String number);



}
