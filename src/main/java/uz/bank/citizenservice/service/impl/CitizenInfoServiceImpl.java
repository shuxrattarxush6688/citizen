package uz.bank.citizenservice.service.impl;

import org.springframework.stereotype.Service;
import uz.bank.citizenservice.dto.CitizenDto;
import uz.bank.citizenservice.entity.Citizen;
import uz.bank.citizenservice.repository.CitizenRepository;
import uz.bank.citizenservice.service.CitizenInfoService;

@Service
public class CitizenInfoServiceImpl implements CitizenInfoService {

    private final CitizenRepository repository;

    public CitizenInfoServiceImpl(CitizenRepository repository) {
        this.repository = repository;
    }

    @Override
    public void add(CitizenDto dto) {
        repository.save(CitizenDto.toEntity(dto));
    }

    @Override
    public CitizenDto findOne(String serial, String number) {
        Citizen citizen = repository.findCitizenByPassportSeriesAndPassportNumber(serial, number);
        if (citizen != null) {
            return CitizenDto.toDto(citizen);
        } else return null;

    }

}
